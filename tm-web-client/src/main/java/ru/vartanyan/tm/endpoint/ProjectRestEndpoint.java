package ru.vartanyan.tm.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.vartanyan.tm.api.IProjectRestEndpoint;
import ru.vartanyan.tm.model.Project;
import ru.vartanyan.tm.repository.ProjectRepository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping("/api/projects")
public class ProjectRestEndpoint implements IProjectRestEndpoint {

    @Autowired
    private ProjectRepository repository;

    @Override
    @GetMapping("/findAll")
    public List<Project> findAll() {
        return new ArrayList<>(repository.findAll());
    }

    @Override
    @GetMapping("/find/{id}")
    public Project find(@PathVariable("id") String id) {
        return repository.findById(id);
    }

    @Override
    @PostMapping("/create")
    public Project create(@RequestBody Project project) {
        repository.save(project);
        return project;
    }

    @Override
    @PostMapping("/createAll")
    public List<Project> createAll(@RequestBody List<Project> projects) {
        for (Project project: projects) {
            repository.save(project);
        }
        return projects;
    }

    @Override
    @PostMapping("/save")
    public Project save(@RequestBody Project project) {
        repository.save(project);
        return project;
    }

    @Override
    @PostMapping("/saveAll")
    public List<Project> saveAll(@RequestBody List<Project> projects) {
        for (Project project: projects) {
            repository.save(project);
        }
        return projects;
    }

    @Override
    @PostMapping("/delete/{id}")
    public void delete(@PathVariable("id") String id) {
        repository.removeById(id);
    }

    @Override
    @PostMapping("/deleteAll")
    public void deleteAll() {
        final Collection<Project> projects = repository.findAll();
        for (Project project: projects) {
            repository.removeById(project.getId());
        }
    }

}
