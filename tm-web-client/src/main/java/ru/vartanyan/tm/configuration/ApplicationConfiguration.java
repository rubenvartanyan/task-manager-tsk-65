package ru.vartanyan.tm.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("ru.vartanyan.tm")
public class ApplicationConfiguration {
}
