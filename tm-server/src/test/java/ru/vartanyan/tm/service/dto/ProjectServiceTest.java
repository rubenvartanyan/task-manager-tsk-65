package ru.vartanyan.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.vartanyan.tm.api.service.dto.IProjectService;
import ru.vartanyan.tm.api.service.dto.ITaskService;
import ru.vartanyan.tm.api.service.dto.IUserService;
import ru.vartanyan.tm.configuration.ServerConfiguration;
import ru.vartanyan.tm.dto.Project;
import ru.vartanyan.tm.dto.User;
import ru.vartanyan.tm.marker.DBCategory;
import ru.vartanyan.tm.service.TestUtil;

import java.util.ArrayList;
import java.util.List;

public class ProjectServiceTest {

    @NotNull AnnotationConfigApplicationContext context =
            new AnnotationConfigApplicationContext(ServerConfiguration.class);

    @NotNull
    private final IProjectService projectService = context.getBean(IProjectService.class);

    @NotNull
    private final IUserService userService = context.getBean(IUserService.class);

    @NotNull
    private final ITaskService taskService = context.getBean(ITaskService.class);

    {
        userService.deleteAll();
        TestUtil.initUser();
    }

    @Test
    @Category(DBCategory.class)
    public void addAllTest() {
        final List<Project> projects = new ArrayList<>();
        final Project project1 = new Project();
        final Project project2 = new Project();
        projects.add(project1);
        projects.add(project2);
        projectService.addAll(projects);
        Assert.assertTrue(projectService.findOneById(project1.getId()) != null);
        Assert.assertTrue(projectService.findOneById(project2.getId()) != null);
        projectService.remove(projects.get(0));
        projectService.remove(projects.get(1));
    }

    @Test
    @Category(DBCategory.class)
    public void addTest() {
        final Project project = new Project();
        projectService.add(project);
        Assert.assertNotNull(projectService.findOneById(project.getId()));
        projectService.remove(project);
    }

    @Test
    @Category(DBCategory.class)
    public void clearTest() {
        taskService.deleteAll();
        projectService.deleteAll();
        Assert.assertTrue(projectService.findAll().isEmpty());
    }

    @Test
    @Category(DBCategory.class)
    public void findAll() {
        final int projectSize = projectService.findAll().size();
        final List<Project> projects = new ArrayList<>();
        final Project project1 = new Project();
        final Project project2 = new Project();
        projects.add(project1);
        projects.add(project2);
        projectService.addAll(projects);
        Assert.assertEquals(2 + projectSize, projectService.findAll().size());
        projectService.remove(project1);
        projectService.remove(project2);
    }

    @Test
    @Category(DBCategory.class)
    public void findOneByIdTest() {
        final Project project = new Project();
        final String projectId = project.getId();
        projectService.add(project);
        Assert.assertNotNull(projectService.findOneById(projectId));
        projectService.remove(project);
    }

    @Test
    @Category(DBCategory.class)
    public void findOneByIndexTest() {
        final Project project = new Project();
        projectService.add(project);
        final String projectId = project.getId();
        Assert.assertTrue(projectService.findOneById(projectId) != null);
        projectService.remove(project);
    }

    @Test
    @Category(DBCategory.class)
    public void findOneByIndexTestByUserId() {
        final Project project = new Project();
        final @Nullable User user = userService.findOneByLogin("test");
        final String userId = user.getId();
        project.setUserId(userId);
        projectService.add(project);
        final String projectId = project.getId();
        Assert.assertTrue(projectService.findOneById(projectId, userId) != null);
        projectService.remove(project);
    }

    @Test
    @Category(DBCategory.class)
    public void findOneByNameTest() {
        final Project project = new Project();
        final @Nullable User user = userService.findOneByLogin("test");
        final String userId = user.getId();
        project.setUserId(userId);
        project.setName("pr1");
        projectService.add(project);
        final String name = project.getName();
        Assert.assertNotNull(name);
        Assert.assertTrue(projectService.findOneByNameAndUserId(name, userId) != null);
        projectService.remove(project);
    }

    @Test
    @Category(DBCategory.class)
    public void removeOneByIdTest() {
        final Project project = new Project();
        projectService.add(project);
        final String projectId = project.getId();
        projectService.removeOneById(projectId);
        Assert.assertFalse(projectService.findOneById(projectId) != null);
    }


    @Test
    @Category(DBCategory.class)
    public void removeOneByIdTestByUserId() {
        final Project project = new Project();
        final @NotNull User user = userService.findOneByLogin("test");
        final String userId = user.getId();
        project.setUserId(userId);
        projectService.add(project);
        final String projectId = project.getId();
        projectService.removeOneById(userId, projectId);
        Assert.assertFalse(projectService.findOneById(userId, projectId) != null);
    }

    @Test
    @Category(DBCategory.class)
    public void removeOneByIndexTest() {
        final Project project1 = new Project();
        final Project project2 = new Project();
        final Project project3 = new Project();
        final @NotNull User user = userService.findOneByLogin("test");
        final String userId = user.getId();
        project1.setUserId(userId);
        project2.setUserId(userId);
        project3.setUserId(userId);
        projectService.add(project1);
        projectService.add(project2);
        projectService.add(project3);
        Assert.assertTrue(projectService.findOneByIndex(userId, 0) != null);
        Assert.assertTrue(projectService.findOneByIndex(userId, 1) != null);
        Assert.assertTrue(projectService.findOneByIndex(userId, 2) != null);
    }

    @Test
    @Category(DBCategory.class)
    public void removeOneByNameTest() {
        final Project project = new Project();
        final @NotNull User user = userService.findOneByLogin("test");
        final String userId = user.getId();
        project.setUserId(userId);
        project.setName("pr1");
        projectService.add(project);
        final String name = project.getName();
        Assert.assertNotNull(name);
        projectService.removeOneByName(userId, name);
        Assert.assertFalse(projectService.findOneByNameAndUserId(userId, name) != null);
    }

    @Test
    @Category(DBCategory.class)
    public void removeTest() {
        final Project project = new Project();
        projectService.add(project);
        projectService.remove(project);
        Assert.assertNull(projectService.findOneById(project.getId()));
    }

    @Test
    @Category(DBCategory.class)
    public void removeTestByUserIdAndObject() {
        final Project project = new Project();
        final @NotNull User user = userService.findOneByLogin("test");
        final String userId = user.getId();
        project.setUserId(userId);
        projectService.add(project);
        projectService.remove(userId, project);
        Assert.assertFalse(projectService.findOneById(userId, project.getId()) != null);
    }
}
